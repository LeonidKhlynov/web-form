const webFormId = '#web-form';

let webForm =
{
    init: function()
    {
        // Inits
        this.initAddressSelector();
        this.initPhoneInputMask();
        this.initAddressMap();


        // Events
        this.setFormCaptchaEvent();
        this.setFormSubmitEvent();
    },


    /**
     * Событие по установке кпчи при клике на кнопку "Отправить". Защита от спама
     */
    setFormCaptchaEvent: function()
    {
        // Отправка формы
        $(document).on('click', webFormId + ' [type="submit"]', function()
        {
            let form    = $(this).closest('form');
            let captcha = form.find('#captcha');

            captcha.val('webFormAllowed');
            form.submit();

            return false;
        });
    },


    /**
     * Событие по отправке формы
     */
    setFormSubmitEvent: function()
    {
        $(document).on('submit', webFormId, function(){
            let form      = $(this);
            let formData  = new FormData(form[0]);
            let errors    = webForm.validateFormInputs(form);
            let submitBtn = form.find('[type="submit"]');

            if (!errors.length)
            {
                submitBtn.text('Идет отправка...');

                setTimeout(function(){
                    $.ajax({
                        url: 'form-send.php',
                        data: formData,
                        type: 'POST',
                        dataType: 'JSON',
                        contentType: false,
                        processData: false,
                    }).done(function(resp){
                        submitBtn.text('Отправить');
                        webForm.showModalMessage(resp.message);
                        webForm.clearInputValues(form);
                    });
                }, 1000);
            }

            return false;
        });
    },


    /**
     * Init: Подсказка для ввода адреса
     */
    initAddressSelector: function()
    {
        // Подсказки при вводе адреса
        let options = {
            //Наше поле, куда пользователи будут вводить почтовые адреса
            id: "address1",

            //Не будем показывать предупреждающее сообщение, когда подсказок нет
            empty_msg : "",

            //Будем показывать только 5 топовых подсказок
            limit : 5,

            //Не будем показывать подсказки при возврате фокуса в поле ввода
            suggest_on_focus : false,

            //При выборе подсказки будем выводить её в отладочную консоль
            on_fetch : function( Suggestion, Address ) {
                //console.log( Suggestion, Address );
            },
        };
        AhunterSuggest.Address.Solid(options);
    },


    /**
     * Init: Маски на input с телефоном
     */
    initPhoneInputMask: function()
    {
        // Маска для телефона
        $('#phone').intlTelInput({
            defaultCountry: 'RU',
            utilsScript: "assets/plugins/intl-tel-input/js/utils.js",
            autoFormat: true,
        });
    },


    /**
     * Init: Yandex Map API для адреса
     */
    initAddressMap: function()
    {
        // Yandex Map API
        ymaps.ready(init);
        function init()
        {
            let myPlacemark,
                myMap = new ymaps.Map('address2_map', {
                    center: [55.753994, 37.622093],
                    zoom: 9,
                    controls: ['zoomControl', 'searchControl']
                }, {
                    searchControlProvider: 'yandex#search'
                });

            // Слушаем клик на карте.
            myMap.events.add('click', function (e) {
                let coords = e.get('coords');

                // Если метка уже создана – просто передвигаем ее.
                if (myPlacemark) {
                    myPlacemark.geometry.setCoordinates(coords);
                }
                // Если нет – создаем.
                else {
                    myPlacemark = createPlacemark(coords);
                    myMap.geoObjects.add(myPlacemark);
                    // Слушаем событие окончания перетаскивания на метке.
                    myPlacemark.events.add('dragend', function () {
                        getAddress(myPlacemark.geometry.getCoordinates());
                    });
                }
                getAddress(coords);
            });

            // Создание метки.
            function createPlacemark(coords) {
                return new ymaps.Placemark(coords, {
                    iconCaption: 'поиск...'
                }, {
                    preset: 'islands#violetDotIconWithCaption',
                    draggable: true
                });
            }

            // Определяем адрес по координатам (обратное геокодирование).
            function getAddress(coords) {
                myPlacemark.properties.set('iconCaption', 'поиск...');
                ymaps.geocode(coords).then(function (res) {
                    let firstGeoObject = res.geoObjects.get(0);
                    $(document).find('#address2').val(firstGeoObject.getAddressLine());


                    myPlacemark.properties
                        .set({
                            // Формируем строку с данными об объекте.
                            iconCaption: [
                                // Название населенного пункта или вышестоящее административно-территориальное образование.
                                firstGeoObject.getLocalities().length ? firstGeoObject.getLocalities() : firstGeoObject.getAdministrativeAreas(),
                                // Получаем путь до топонима, если метод вернул null, запрашиваем наименование здания.
                                firstGeoObject.getThoroughfare() || firstGeoObject.getPremise()
                            ].filter(Boolean).join(', '),
                            // В качестве контента балуна задаем строку с адресом объекта.
                            balloonContent: firstGeoObject.getAddressLine()
                        });
                });
            }
        }
    },


    /**
     * Валидация формы
     *
     * @param object form
     *
     * @returns {*[]}
     */
    validateFormInputs: function(form)
    {
        let errors   = [];
        let phone    = form.find('#phone');
        let name     = form.find('#name');
        let required = form.find('[required]');

        // Проверка на пустые значения
        required.each(function(i, elem){
            if ($(elem).val() === '')
            {
                errors.push($(elem));
                $(elem).closest('.form__item').addClass('has-error empty-error');
            }
        });

        // Проверка валидности телефона
        if (!phone.intlTelInput('isValidNumber') && phone.val() !== '')
        {
            errors.push(phone);
            phone.closest('.form__item').addClass('has-error phone-error');
        }

        // Проверка валидности имени
        if (!name.val().match(/^[А-Яа-яa-z -]+$/g) && name.val() !== '')
        {
            errors.push(name);
            name.closest('.form__item').addClass('has-error text-error');
        }

        // Очищаем ошибки через пару секунж
        this.clearErrorMassages(required);


        return errors;
    },


    /**
     * Скрытие ошибок формы
     *
     * @param inputs
     */
    clearErrorMassages: function(inputs)
    {
        setTimeout(function(){
            inputs.closest('.form__item').removeClass('has-error empty-error phone-error text-error');
        }, 1000 * 5);
    },


    /**
     * Очистка input в форме
     *
     * @param form
     */
    clearInputValues: function(form)
    {
        form.find('input').val('');
    },


    /**
     * Показать модальное окно с успешной отправкой формы
     */
    showModalMessage: function(text)
    {
        let modal = $('#message-response');

        modal.text(text);
        modal.fancybox({
            beforeShow : function(){
                this.title =  $(this.element).data("caption");
            },
            buttons: [
                "fullScreen",
                "thumbs",
                "close",
            ],
            clickContent : false,
            loop: false,
            touch: false,
            transitionEffect: "fade",
            animationEffect: "fade",
        }).click();
    }
};