<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Web-form</title>

    <!-- Yandex Map API  -->
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=820292ae-fb77-4427-b25c-9081f7b29909" type="text/javascript"></script>
    <!-- /Yandex Map API  -->

    <!-- ASSETS -->
    <link rel="stylesheet" href="assets/plugins/intl-tel-input/css/intlTelInput.css">
    <link rel="stylesheet" href="assets/plugins/fancybox/fancybox.css">
    <link rel="stylesheet" href="assets/css/web-form.css">


    <script src="assets/plugins/jquery/jquery.min.js"></script>
    <script src="assets/plugins/fancybox/fancybox.js"></script>
    <script src="assets/plugins/intl-tel-input/js/intlTelInput.js"></script>
    <script src="assets/plugins/address-suggest/address-suggest.js"></script>

    <script src="assets/js/CSlWebForm.js"></script>
    <script src="assets/js/web-form.js"></script>
    <!-- /ASSETS -->

    <?php
    require_once (__DIR__.'/CSlHelper.php');
    ?>
</head>
<body>
    <section class="form__section">
        <div class="container">
            <div class="form__block">
                <form action="" class="form" id="web-form">
                    <input type="hidden" name="ip" value="<?= CSlHelper::getClientIP(); ?>">
                    <input type="hidden" name="captcha" value="" id="captcha">
                    <div class="form__item">
                        <label for="name">Имя</label>
                        <input type="text" name="name" maxlength="25" id="name">
                        <div class="form__errorMessage" data-error="text">Имя может содержать только буквы и тире</div>
                    </div>
                    <div class="form__item">
                        <label for="phone">Телефон<span>*</span></label>
                        <input type="tel" name="phone" maxlength="25" id="phone" required>
                        <div class="form__errorMessage" data-error="empty">Телефон не заполнен</div>
                        <div class="form__errorMessage" data-error="phone">Телефон заполнен некорректно</div>
                    </div>
                    <div class="form__item">
                        <label for="address1">Адрес 1<span>*</span></label>
                        <input type="text" name="address1" id="address1" required>
                        <div class="form__errorMessage" data-error="empty">Адрес 1 не заполнен</div>
                    </div>
                    <div class="form__item">
                        <label for="address2">Адрес 2<span>*</span></label>
                        <div id="address2_map"></div>
                        <input type="hidden" name="address2" id="address2" required>
                        <div class="form__errorMessage" data-error="empty">Адрес 2 не выбран</div>
                    </div>
                    <div class="form__footer">
                        <button type="submit">Отправить</button>
                    </div>
                </form>
            </div>
        </div>
    </section>

    <div id="message-response" class="modalBlock" style="display: none;">
    </div>
</body>
</html>