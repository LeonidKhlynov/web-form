<?php
require_once (__DIR__.'/CSlHelper.php');

$response   = ['status' => false, 'message' => 'Ошибка. Не удалось отправить данные формы'];
$defCaptcha = 'webFormAllowed';

$address1   = $_POST['address1'];
$address2   = $_POST['address2'];
$name       = $_POST['name'];
$phone      = $_POST['phone'];
$captcha    = $_POST['captcha'];
$ip         = $_POST['ip'];

if ($captcha == $defCaptcha && !empty($phone) && !empty($address1) && !empty($address2))
{
    // Записываем данные в лог
    CSlHelper::setFormDataToLog($_POST);
    $response = ['status' => true, 'message' => 'Ок. Данные формы успешно отправлены'];
}

echo json_encode($response);
die();