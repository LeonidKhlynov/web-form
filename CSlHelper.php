<?php
/**
 * @author Leonid A Khlynov <dev@goodboy.com>
 * @license MIT
 * @version 1.0
 */


class CSlHelper
{
    /**
     * Получение IP адреса пользователя
     *
     * @return mixed|string
     */
    public static function getClientIP()
    {
        $keys = ['HTTP_CLIENT_IP','HTTP_X_FORWARDED_FOR','HTTP_X_FORWARDED','HTTP_FORWARDED_FOR','HTTP_FORWARDED','REMOTE_ADDR'];
        foreach($keys as $k)
        {
            if (!empty($_SERVER[$k]) && filter_var($_SERVER[$k], FILTER_VALIDATE_IP))
            {
                return $_SERVER[$k];
            }
        }
        return "UNKNOWN";
    }


    /**
     * Запись результата формы в лог
     *
     * @param $formData
     *
     * @return false|int
     */
    public static function setFormDataToLog($formData)
    {
        $logItem = [
            'ip'       => $formData['ip'],
            'name'     => $formData['name'],
            'phone'    => $formData['phone'],
            'address1' => $formData['address1'],
            'address2' => $formData['address2'],
            'date'     => date('Y-m-d H:i:s')
        ];

        return file_put_contents(__DIR__.'/logform.txt', var_export($logItem, true).PHP_EOL.PHP_EOL, FILE_APPEND);
    }
}